package ru.samsu.ibaryshnikov.diary;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    private Button button;
    private int PICK_REQUEST_CODE = 1;
    private ArrayList<ListEntry> entries;
    private ListEntryAdapter listEntryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        entries = new ArrayList<>();
        listEntryAdapter = new ListEntryAdapter(this, entries);

        button = (Button) findViewById(R.id.activity_main_add_entry_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, PickActivity.class);
                startActivityForResult(intent, PICK_REQUEST_CODE);
            }
        });

        ListView listView = (ListView) findViewById(R.id.activity_main_list_view);
        listView.setAdapter(listEntryAdapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_REQUEST_CODE && resultCode == RESULT_OK) {
            String description = data.getStringExtra(PickActivity.DESCRIPTION_EXTRA_KEY);
            String content = data.getStringExtra(PickActivity.CONTENT_EXTRA_KEY);

            Toast.makeText(this, description + "/" + content, Toast.LENGTH_LONG).show();
            ListEntry listEntry = new ListEntry(description, content);
            listEntryAdapter.add(listEntry);
        }
    }

    private class ListEntryAdapter extends ArrayAdapter<ListEntry> {
        public ListEntryAdapter(Context context, ArrayList<ListEntry> users) {
            super(context, 0, users);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_entry, parent, false);
            }
            TextView descriptionTextView = (TextView) convertView.findViewById(R.id.list_entry_description_text_view);
            TextView contentTextView = (TextView) convertView.findViewById(R.id.list_entry_content_text_view);
            ListEntry listEntry = getItem(position);
            descriptionTextView.setText(listEntry.DESCRIPTION);
            contentTextView.setText(listEntry.CONTENT);
            return convertView;
        }
    }
}