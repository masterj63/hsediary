package ru.samsu.ibaryshnikov.diary;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class PickActivity extends AppCompatActivity {
    static final String DESCRIPTION_EXTRA_KEY = "ru.samsu.ibaryshnikov.diary.PickActivity.DESCRIPTION_EXTRA_KEY";
    static final String CONTENT_EXTRA_KEY = "ru.samsu.ibaryshnikov.diary.PickActivity.CONTENT_EXTRA_KEY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick);

        final EditText descriptionEditText = (EditText) findViewById(R.id.activity_pick_description_edit_text);
        final EditText contentEditText = (EditText) findViewById(R.id.activity_pick_content_edit_text);

        Button pickButton = (Button) findViewById(R.id.activity_pick_ok_button);
        pickButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra(DESCRIPTION_EXTRA_KEY, descriptionEditText.getText().toString());
                intent.putExtra(CONTENT_EXTRA_KEY, contentEditText.getText().toString());
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}
