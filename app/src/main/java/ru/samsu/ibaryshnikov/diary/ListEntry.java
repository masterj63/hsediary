package ru.samsu.ibaryshnikov.diary;

public class ListEntry {
    public final String DESCRIPTION;
    public final String CONTENT;

    public ListEntry(String description, String content) {
        DESCRIPTION = description;
        CONTENT = content;
    }
}
